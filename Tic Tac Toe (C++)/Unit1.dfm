object Form1: TForm1
  Left = 148
  Top = 106
  Width = 985
  Height = 524
  Caption = 'SpaceWars v.1.0'
  Color = cl3DDkShadow
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -37
  Font.Name = 'Lucida Console'
  Font.Style = [fsBold]
  Menu = MainMenu1
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 37
  object Background: TImage
    Left = 0
    Top = 0
    Width = 969
    Height = 465
    Align = alClient
    Stretch = True
  end
  object Image1: TImage
    Left = 40
    Top = 40
    Width = 130
    Height = 130
    Cursor = crHandPoint
    Center = True
    Transparent = True
    OnClick = Image1Click
  end
  object Image2: TImage
    Left = 168
    Top = 40
    Width = 130
    Height = 130
    Cursor = crHandPoint
    Center = True
    Transparent = True
    OnClick = Image2Click
  end
  object Image3: TImage
    Left = 296
    Top = 40
    Width = 130
    Height = 130
    Cursor = crHandPoint
    Center = True
    Transparent = True
    OnClick = Image3Click
  end
  object Image4: TImage
    Left = 40
    Top = 168
    Width = 130
    Height = 130
    Cursor = crHandPoint
    Center = True
    Transparent = True
    OnClick = Image4Click
  end
  object Image5: TImage
    Left = 168
    Top = 168
    Width = 130
    Height = 130
    Cursor = crHandPoint
    Center = True
    Transparent = True
    OnClick = Image5Click
  end
  object Image6: TImage
    Left = 296
    Top = 168
    Width = 130
    Height = 130
    Cursor = crHandPoint
    Center = True
    Transparent = True
    OnClick = Image6Click
  end
  object Image7: TImage
    Left = 40
    Top = 296
    Width = 130
    Height = 130
    Cursor = crHandPoint
    Center = True
    Transparent = True
    OnClick = Image7Click
  end
  object Image8: TImage
    Left = 168
    Top = 296
    Width = 130
    Height = 130
    Cursor = crHandPoint
    Center = True
    Transparent = True
    OnClick = Image8Click
  end
  object Image9: TImage
    Left = 296
    Top = 296
    Width = 130
    Height = 130
    Cursor = crHandPoint
    Center = True
    Transparent = True
    OnClick = Image9Click
  end
  object Label1: TLabel
    Left = 528
    Top = 104
    Width = 230
    Height = 37
    Caption = 'NEXT TURN:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -37
    Font.Name = 'Lucida Console'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Turn: TImage
    Left = 816
    Top = 96
    Width = 50
    Height = 50
    Transparent = True
  end
  object Venus: TImage
    Left = 536
    Top = 256
    Width = 50
    Height = 50
    Transparent = True
  end
  object Mars: TImage
    Left = 536
    Top = 336
    Width = 50
    Height = 50
    Transparent = True
  end
  object Label2: TLabel
    Left = 616
    Top = 208
    Width = 72
    Height = 29
    Caption = 'WINS'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -29
    Font.Name = 'Lucida Console'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label3: TLabel
    Left = 712
    Top = 208
    Width = 90
    Height = 29
    Caption = 'DRAWS'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -29
    Font.Name = 'Lucida Console'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label4: TLabel
    Left = 824
    Top = 208
    Width = 108
    Height = 29
    Caption = 'LOSSES'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -29
    Font.Name = 'Lucida Console'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object VW: TLabel
    Left = 640
    Top = 264
    Width = 18
    Height = 29
    Caption = '0'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -29
    Font.Name = 'Lucida Console'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object VD: TLabel
    Left = 752
    Top = 264
    Width = 18
    Height = 29
    Caption = '0'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -29
    Font.Name = 'Lucida Console'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object VL: TLabel
    Left = 864
    Top = 264
    Width = 18
    Height = 29
    Caption = '0'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -29
    Font.Name = 'Lucida Console'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object MW: TLabel
    Left = 640
    Top = 344
    Width = 18
    Height = 29
    Caption = '0'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -29
    Font.Name = 'Lucida Console'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object MD: TLabel
    Left = 752
    Top = 344
    Width = 18
    Height = 29
    Caption = '0'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -29
    Font.Name = 'Lucida Console'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object ML: TLabel
    Left = 864
    Top = 344
    Width = 18
    Height = 29
    Caption = '0'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -29
    Font.Name = 'Lucida Console'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object MainMenu1: TMainMenu
    Left = 928
    Top = 16
    object Mode1: TMenuItem
      Caption = '&Mode'
      object Restart1: TMenuItem
        Caption = '&Restart     Ctrl+R'
        OnClick = Restart1Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object N3x31: TMenuItem
        Caption = '3x3'
        Checked = True
      end
      object N4x41: TMenuItem
        Caption = '4x4'
        Enabled = False
      end
      object N5x51: TMenuItem
        Caption = '5x5'
        Enabled = False
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object Close1: TMenuItem
        Caption = '&Close     Ctrl+E'
        OnClick = Close1Click
      end
    end
    object Language1: TMenuItem
      Caption = '&Language'
      object English1: TMenuItem
        Caption = 'English'
        Checked = True
        Default = True
      end
      object Polish1: TMenuItem
        Caption = 'Polish'
        Enabled = False
      end
      object Swedish1: TMenuItem
        Caption = 'Swedish'
        Enabled = False
      end
    end
    object Help1: TMenuItem
      Caption = '&Help'
      object Wiki1: TMenuItem
        Caption = '&Information'
        object EN1: TMenuItem
          Caption = 'EN'
        end
        object PL1: TMenuItem
          Caption = 'PL'
        end
        object SV1: TMenuItem
          Caption = 'SV'
        end
      end
      object N3: TMenuItem
        Caption = '-'
      end
      object About1: TMenuItem
        Caption = '&About...'
      end
    end
  end
end
