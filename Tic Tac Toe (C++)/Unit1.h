//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <jpeg.hpp>
#include <Menus.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TImage *Image1;
        TImage *Image2;
        TImage *Image3;
        TImage *Image4;
        TImage *Image5;
        TImage *Image6;
        TImage *Image7;
        TImage *Image8;
        TImage *Image9;
        TLabel *Label1;
        TImage *Turn;
        TImage *Background;
        TImage *Venus;
        TImage *Mars;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *Label4;
        TLabel *VW;
        TLabel *VD;
        TLabel *VL;
        TLabel *MW;
        TLabel *MD;
        TLabel *ML;
        TMainMenu *MainMenu1;
        TMenuItem *Mode1;
        TMenuItem *Restart1;
        TMenuItem *N3x31;
        TMenuItem *N4x41;
        TMenuItem *N5x51;
        TMenuItem *Close1;
        TMenuItem *Language1;
        TMenuItem *English1;
        TMenuItem *Polish1;
        TMenuItem *Swedish1;
        TMenuItem *Help1;
        TMenuItem *Wiki1;
        TMenuItem *About1;
        TMenuItem *EN1;
        TMenuItem *PL1;
        TMenuItem *SV1;
        TMenuItem *N1;
        TMenuItem *N2;
        TMenuItem *N3;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall Image1Click(TObject *Sender);
        void __fastcall Image2Click(TObject *Sender);
        void __fastcall Image3Click(TObject *Sender);
        void __fastcall Image4Click(TObject *Sender);
        void __fastcall Image5Click(TObject *Sender);
        void __fastcall Image6Click(TObject *Sender);
        void __fastcall Image7Click(TObject *Sender);
        void __fastcall Image8Click(TObject *Sender);
        void __fastcall Image9Click(TObject *Sender);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall Restart1Click(TObject *Sender);
        void __fastcall Close1Click(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
private:	// User declarations
public:		// User declarations
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
 