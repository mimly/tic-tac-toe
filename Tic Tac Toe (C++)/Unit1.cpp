//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;

TImage *Images[9];
char f[9]; //'V' like Venus, 'M' like Mars and N like 'Nothing'
char state;
bool tie;
int wins = 0, draws = 0, losses = 0; //from Venus perspective

void checkWinner(TObject *Sender) {
        if ((f[0]==f[1] && f[1]==f[2] && f[0]!='N') ||
        (f[3]==f[4] && f[4]==f[5] && f[3]!='N') ||
        (f[6]==f[7] && f[7]==f[8] && f[6]!='N') ||
        (f[0]==f[3] && f[3]==f[6] && f[0]!='N') ||
        (f[1]==f[4] && f[4]==f[7] && f[1]!='N') ||
        (f[2]==f[5] && f[5]==f[8] && f[2]!='N') ||
        (f[0]==f[4] && f[4]==f[8] && f[0]!='N') ||
        (f[2]==f[4] && f[4]==f[6] && f[2]!='N')) {
                char *txt;
                if (state == 'M') {
                        txt = "Venus wins the game!";
                        //Vwins++;
                        //Mlosses++;
                        wins++;
                        Form1->VW->Caption = wins;
                        Form1->ML->Caption = wins;
                }
                else {
                        txt = "Mars wins the game!";
                        //Vlosses++;
                        //Mwins++;
                        losses++;
                        Form1->VL->Caption = losses;
                        Form1->MW->Caption = losses;
                }
                int msgbox = MessageBoxA(NULL, txt, "GAME OVER", MB_OK | MB_ICONINFORMATION | MB_DEFBUTTON1 | MB_TASKMODAL);
                if (msgbox == IDOK) Form1->FormCreate(Sender);
        }
        tie = true;
        for (int i=0; i<(sizeof(f)/sizeof(*f)); i++) {
                if (f[i] == 'N') tie = false;
        }
        if (tie) {
                char *txt = "No winner, no looser! Draw!";
                draws++;
                Form1->VD->Caption = draws;
                Form1->MD->Caption = draws;
                int msgbox = MessageBoxA(NULL, txt, "GAME OVER", MB_OK | MB_ICONINFORMATION | MB_DEFBUTTON1 | MB_TASKMODAL);
                if (msgbox == IDOK) Form1->FormCreate(Sender);
        }
}

void makeMove(TObject *Sender, int i) {
        if (f[i]=='N') {
                if (state=='V') {
                        Images[i]->Picture->LoadFromFile("img/Venus.bmp");
                        f[i] = 'V';
                        state = 'M';
                        Form1->Turn->Picture->LoadFromFile("img/MarsS.bmp");
                }
                else {
                        Images[i]->Picture->LoadFromFile("img/Mars.bmp");
                        f[i] = 'M';
                        state = 'V';
                        Form1->Turn->Picture->LoadFromFile("img/VenusS.bmp");
                }
                Images[i]->Enabled = false;
                checkWinner(Sender);
        }
}

//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject *Sender)
{
        for (int i=0; i<(sizeof(f)/sizeof(*f)); i++) {
                f[i] = 'N';
        }
        state = 'V'; // first-move

        Images[0] = Image1; Images[1] = Image2; Images[2] = Image3;
        Images[3] = Image4; Images[4] = Image5; Images[5] = Image6;
        Images[6] = Image7; Images[7] = Image8; Images[8] = Image9;

        Background->Picture->LoadFromFile("img/Background.bmp");
        for (int i=0; i<(sizeof(Images)/sizeof(*Images)); i++) {
                Images[i]->Picture->LoadFromFile("img/None.bmp");
                Images[i]->Enabled = true;
        }
        Turn->Picture->LoadFromFile("img/VenusS.bmp");
        Venus->Picture->LoadFromFile("img/VenusS.bmp");
        Mars->Picture->LoadFromFile("img/MarsS.bmp");
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Image1Click(TObject *Sender)
{
        makeMove(Sender, 0);
}
//---------------------------------------------------------------------------


void __fastcall TForm1::Image2Click(TObject *Sender)
{
        makeMove(Sender, 1);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Image3Click(TObject *Sender)
{
        makeMove(Sender, 2);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Image4Click(TObject *Sender)
{
        makeMove(Sender, 3);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Image5Click(TObject *Sender)
{
        makeMove(Sender, 4);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Image6Click(TObject *Sender)
{
        makeMove(Sender, 5);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Image7Click(TObject *Sender)
{
        makeMove(Sender, 6);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Image8Click(TObject *Sender)
{
        makeMove(Sender, 7);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Image9Click(TObject *Sender)
{
        makeMove(Sender, 8);
}
//---------------------------------------------------------------------------


void __fastcall TForm1::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
        if (Shift.Contains(ssCtrl)) {
                if ((Key == 'r') || (Key == 'R')) {
                        Form1->Restart1Click(Sender);
                }
                else if ((Key == 'e') || (Key == 'E')) {
                        Form1->Close1Click(Sender);
                }
        }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Restart1Click(TObject *Sender)
{
        wins = 0; draws = 0; losses = 0;
        Form1->VW->Caption = wins;
        Form1->ML->Caption = wins;
        Form1->VL->Caption = losses;
        Form1->MW->Caption = losses;
        Form1->VD->Caption = draws;
        Form1->MD->Caption = draws;
        Form1->FormCreate(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Close1Click(TObject *Sender)
{
        //Form1->Close(); //it will close only one window
        if (Application->MessageBoxA("Do You really wanna leave me this way?", "CONFIRM", MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2 | MB_TASKMODAL)
        == IDYES) {
                Application->Terminate();
        }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormClose(TObject *Sender, TCloseAction &Action)
{
        //Form1->Close(); //it will close only one window
        if (Application->MessageBoxA("Do You really wanna leave me this way?", "CONFIRM", MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2 | MB_TASKMODAL)
        == IDNO) {
                Action=caNone;
        }
}
//---------------------------------------------------------------------------

